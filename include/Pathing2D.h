#pragma once
#include <octomap/octomap.h>
#include <iostream>
#include <cmath>
#include <opencv2/opencv.hpp>
#include "Graph.hpp"
#include "Histogram.hpp" 
#include <util.h>
#include <Eigen/Geometry>
#include <boost/shared_ptr.hpp>


struct Pose : octomath::Pose6D {
    Pose(float x, float y, float z, double roll, double pitch, double yaw) : octomath::Pose6D(x,y,z,roll,pitch,yaw){}

    octomap::point3d position(){
        return octomap::point3d(x(),y(),z());
    }
};


class Pathing2D {
  bool gotGoal, gotOcto, gotRover, isProcessing;
  double ox, oy, oz;
  double mx, my, mz;
  double width, height, depth;
  const char *hostname = "127.0.0.1";
  int port = 6379;
  int isunix = 0;
  std::string frame_id;
  boost::shared_ptr<octomap::OcTree> tree;
  public:
    float res,
          maxBumpiness,
          robotRadius,
          dangerOfUnknown,
          roughnessWeight,
          steepnessWeight,
          maxSteepness,
          roverConnectionRad,
          goalConnectionRad,
          interConnectionRad,
          greed;
    std::vector<size_t> path;
    std::vector<WeightedPoint> pathnodes;
    octomap::point3d destpoint;
    size_t start;
    size_t dest;
    int maxEdges;
    Pose rover;
    Pose goal;
    Pathing2D();
    void octomapCallback(boost::shared_ptr<octomap::OcTree> input_tree);


    void process();
    cv::Mat processOctomap(cv::Mat * unknown);

    float dist2d(octomap::point3d p1, octomap::point3d p2);

    Graph<float> buildGraph(
        const cv::Mat & heightHist,
        const cv::Mat & occupancy,
        std::vector<WeightedPoint> & openPoses, // output
        cv::Mat * graphIndexes);
};