#pragma once
// #include <octomap::point3dh>
#include <opencv2/opencv.hpp>
#include <octomap/octomap.h>

struct WeightedPoint : public octomap::point3d {
    WeightedPoint(float x, float y, float z, float weight) : octomap::point3d(x, y, z) {

    }
  float weight;
  bool operator < (WeightedPoint const &b) { 
    return weight < b.weight;
  }
};

// It's like filter2d but implemented properly
cv::Mat filter2D(cv::Mat src, int ddepth, cv::Mat & kernel,
    const cv::Point & origin=cv::Point(-1,-1), double delta=0,
    int border_type=cv::BORDER_DEFAULT,
    const cv::Scalar & border_value=cv::Scalar());
cv::Mat circularFilter(int rad);
float dist2d(octomap::point3d & a, octomap::point3d & b);
float dist3d(octomap::point3d & a, octomap::point3d & b);

