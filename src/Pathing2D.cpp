#include "Pathing2D.h"
#include <hiredis/hiredis.h>
#include "util.h"
#include <opencv2/opencv.hpp>
#include <cmath>

Pathing2D::Pathing2D() :
  gotOcto(false), gotGoal(false), gotRover(true), isProcessing(false), rover(0,0,0,0,0,0), goal(0,0,0,0,0,0)
{
  frame_id = "map";
  maxEdges = 100000;
  res = .2;
  maxBumpiness = 3;
  robotRadius = .15;
  dangerOfUnknown = 4; // use high value to prevent all unknown locations from being checked
  roughnessWeight = .5;
  steepnessWeight = 1;
  maxSteepness = .25;
  roverConnectionRad = 3;
  goalConnectionRad = 3;
  interConnectionRad = 3;
  greed = 5;
};

// Modified to just take in a pointer to a octmap
void Pathing2D::octomapCallback(boost::shared_ptr<octomap::OcTree> input_tree) {
  if (!isProcessing) {
    tree = input_tree;
    gotOcto = true;
    process();
  }
}

cv::Mat Pathing2D::processOctomap(cv::Mat * unknown) {
  tree->getMetricMin(ox, oy, oz);
  tree->getMetricMax(mx, my, mz);
  tree->getMetricSize(width, height, depth);

  octomath::Vector3 first(ox, oy, oz);
  octomath::Vector3 second(mx, my, mz);

  octomap::OcTreeKey min, max;
  tree->coordToKeyChecked(first, min);
  tree->coordToKeyChecked(second, max);

  octomath::Vector3 minP = tree->keyToCoord(min);
  octomath::Vector3 maxP = tree->keyToCoord(max);

  Histogram<float> occupied (
      int(ceil((maxP.x()-minP.x()+2)/res))+1, int(ceil((maxP.y()-minP.y()+2)/res))+1, 0, 0);
  Histogram<float> empty (
      int(ceil((maxP.x()-minP.x()+2)/res))+1, int(ceil((maxP.y()-minP.y()+2)/res))+1, 0, 0);

  // Fill
  for(auto it = tree->begin_leafs_bbx(min, max); it != tree->end_leafs_bbx(); it++) {
    octomath::Vector3 pos = it.getCoordinate();
    int x = int((pos.x()-minP.x()+1)/res);
    int y = int((pos.y()-minP.y()+1)/res);
    if (it->getValue() > 0) {
      occupied.add(x, y, pos.z());
    } else {
      empty.add(x, y, pos.z());
    }
  }

  cv::Mat occ_min, occ_max, unknownOcc;
  // This crap takes a long time
  occupied.findExtrema(&occ_min, &occ_max, &unknownOcc, dangerOfUnknown);
  cv::Mat emp_min, emp_max, unknownEmp;
  empty.findExtrema(&emp_min, &emp_max, &unknownEmp, dangerOfUnknown);
  cv::Mat hist = cv::min(occ_max, emp_min);
  cv::bitwise_and(unknownOcc, unknownEmp, *unknown);

  /* Display things
  cv::namedWindow("Hist", 0);
  cv::Mat bw;
  cv::normalize(occ_max, bw, 0, 255, 32, CV_8UC1);
  cv::imshow("Hist", bw);
  cv::waitKey(0);
  cv::normalize(emp_min, bw, 0, 255, 32, CV_8UC1);
  cv::imshow("Hist", bw);
  cv::waitKey(0);
  cv::normalize(hist, bw, 0, 255, 32, CV_8UC1);
  cv::imshow("Hist", bw);
  cv::waitKey(0);
  */

  return hist;
}

float Pathing2D::dist2d(octomap::point3d p1, octomap::point3d p2){
  return pow(pow((p1.x() - p2.x()),2) + pow((p1.y() - p2.y()),2), .5);
}


Graph<float> Pathing2D::buildGraph(
    const cv::Mat & heightHist,
    const cv::Mat & occupancy,
    std::vector<WeightedPoint> & openPoses,
    cv::Mat * graphIndexes)
{

  // Create positions filter by threshold
  *graphIndexes = -cv::Mat::ones(occupancy.rows, occupancy.cols, cv::DataType<int>::type);
  WeightedPoint src = WeightedPoint(rover.x(),rover.y(),rover.z(),0);
  // src.x = rover.position().x;
  // src.y = rover.position().y;
  // src.z = rover.position().z;
  openPoses.push_back(src);

  for (int i=0; i<occupancy.rows; i++) {
    for (int j=0; j<occupancy.cols; j++) {
      // Exponentiate threshold to match occupationHist
      if (occupancy.at<float>(i, j) < maxBumpiness) {
        WeightedPoint p = WeightedPoint(
          j * res + ox - 1,
          i * res + oy - 1,
          heightHist.at<float>(i, j)*res,
          occupancy.at<float>(i,j)
        );
        graphIndexes->at<int>(i, j) = openPoses.size();
        openPoses.push_back(p);
      }
    }
  }

  WeightedPoint end = WeightedPoint(goal.position().x(), goal.position().y(), goal.position().z(), 0);
  cout << "TESTING  " << " " << goal.position().x() << " " << goal.position().y() << " " << goal.position().z() << endl;
  openPoses.push_back(end);

  // This prints a crap ton of stuff, not sure what it represents fully yet
  // std::cout << *graphIndexes << std::endl;

  float dist, slope, roughness;
  bool goalWithinHist = (ox < goal.position().x() && goal.position().x() < mx) &&
                        (oy < goal.position().y() && goal.position().y() < my);
  Graph<float> g(openPoses.size(), maxEdges);
  for (int i=0; i<graphIndexes->rows; i++) {
    for (int j=0; j<graphIndexes->cols; j++) {
      if (graphIndexes->at<int>(i, j) != -1) {
        
        int ind1 = graphIndexes->at<int>(i, j);
        WeightedPoint & p1 = openPoses[ind1];
        // Add key edges to graph
        // Rover
        dist = dist2d(p1, rover.position());
        slope = abs(p1.z()-rover.position().z())/dist;
        if (dist < roverConnectionRad && slope < maxSteepness) {
          slope = abs(p1.z()-rover.z())/dist;
          g.addEdge(ind1, 0,
              dist + slope*steepnessWeight + p1.weight*roughnessWeight);
        }
        // Goal
        if (goalWithinHist) {
          dist = dist2d(p1, goal.position());
          slope = abs(p1.z()-goal.position().z())/dist;
          if (slope < maxSteepness && dist < goalConnectionRad) {
            g.addEdge(ind1, openPoses.size()-1,
                dist + slope*steepnessWeight + p1.weight*roughnessWeight);
          }
        }

        // Add local edges to graph
        for (int dy=-1; dy<=1; dy++) {
          for (int dx=-1; dx<=1; dx++) {
            if ((i+dy > 0 && i+dy < occupancy.rows) &&
                (j+dx > 0 && j+dx < occupancy.cols) &&
                (graphIndexes->at<int>(i+dy, j+dx) != -1) &&
                !(dx==0 && dy==0)) {
              int ind2 = graphIndexes->at<int>(i+dy, j+dx);
              WeightedPoint & p2 = openPoses[ind2];

              dist = dist2d(p1, p2);
              slope = abs(p1.z()-p2.z())/dist;
              roughness = p1.weight/2+p2.weight/2;
              if (slope < maxSteepness && dist < interConnectionRad) {
                g.addEdge(ind1, ind2,
                    dist + slope*steepnessWeight + roughness*roughnessWeight);
              }
            }
          }
        }
      }
    }
  }
  return g;
}

void Pathing2D::process() {
  gotGoal = false;
  isProcessing = true;
  std::cout << "Creating heightmap" << std::endl;
  cv::Mat unknown;
  cv::Mat rawHeight = processOctomap(&unknown);
  cv::Mat bw;

  /* Display
  cv::normalize(rawHeight, bw, 0, 255, 32, CV_8UC1);
  cv::namedWindow("Height", 0);
  cv::imshow("Height", bw);
  cv::waitKey(0);
  */
  
  //--------- Calculate slope histogram (derivative) ------------

  cv::Mat edgeHist;
  /// Generate grad_x and grad_y
  cv::Mat grad_x, grad_y;
  cv::Mat abs_grad_x, abs_grad_y;

  /// Preprocessing
  cv::GaussianBlur(rawHeight, edgeHist, cv::Size(3,3), 10, 10,
      cv::BORDER_CONSTANT);//, dangerOfUnknown);

  // /* Display
  // cv::normalize(edgeHist, bw, 0, 255, 32, CV_8UC1);
  // cv::namedWindow("Edge", 0);
  // cv::imshow("Edge", bw);
  // cv::waitKey(0);
  // */

  /// Gradient X
  cv::Sobel( edgeHist, grad_x, -1, 1, 0, 3, 1, 0,
      cv::BORDER_CONSTANT);//, dangerOfUnknown);

  /// Gradient Y
  cv::Sobel( edgeHist, grad_y, -1, 0, 1, 3, 1, 0,
      cv::BORDER_CONSTANT);//, dangerOfUnknown);

  /// Total Gradient (approximate)
  cv::addWeighted( grad_x, 0.5, grad_y, 0.5, 0, edgeHist, -1);
  float avg = float(cv::sum(edgeHist)[0])/edgeHist.rows/edgeHist.cols;
  edgeHist = cv::abs(edgeHist - avg);
  cv::add(edgeHist, unknown*dangerOfUnknown, edgeHist, cv::noArray(), CV_32F);
  // /* Display
  // cv::normalize(edgeHist, bw, 0, 255, 32, CV_8UC1);
  // cv::namedWindow("Edge", 0);
  // cv::imshow("Edge", bw);
  // cv::waitKey(0);
  // */

  //---------------------------END-------------------------------


  //--------- Perform radius search around each node ------------
  // Create circlular filter

  cv::Mat prefiltered;
  cv::Mat occupationHist;
  float THRESHOLD = 10;
  cv::threshold(edgeHist, prefiltered, THRESHOLD, THRESHOLD, 2);

  // Apply filter
  if (robotRadius/res > 1) {
    cv::Mat filter = circularFilter(robotRadius/res);
    filter /= float(cv::sum(filter)[0]); // Average bumpiness
    // If your filter is too big you will get Nans
    occupationHist = filter2D(prefiltered, -1, filter,
        cv::Point(-1, -1), 0, cv::BORDER_CONSTANT, dangerOfUnknown);
  } else {
    occupationHist = prefiltered;
  }

  // /* Display
  // cv::normalize(occupationHist, bw, 0, 255, 32, CV_8UC1);
  // cv::namedWindow("Traversable", 0);
  // cv::imshow("Traversable", bw);
  // cv::waitKey(0);
  // */
  //---------------------------END-------------------------------


  // Find all edges for graph
  std::cout << "Building graph" << std::endl;
  std::vector<WeightedPoint> open;
  cv::Mat graphIndexes;
  Graph<float> g = buildGraph(rawHeight, occupationHist, open, &graphIndexes);

  size_t closestIndex = -1;
  float closest = std::numeric_limits<float>::max();
  // Find closest point to destination in open
  for(int i = 0; i < open.size(); i++) {
    float dist = pow(pow(open[i].x() - destpoint.x(), 2) + 
                              pow(open[i].y() - destpoint.y(), 2) + 
                              pow(open[i].z() - destpoint.z(), 2), .5);
    if(dist < closest) {
      closest = dist;
      closestIndex = i;
    }
  }
  dest = closestIndex;

  // Find path
  std::vector<size_t> shortestPath = g.shortestPath(start, dest, greed, open);
  g.saveDotFile("graph.gv");
  path = shortestPath;
  isProcessing = false;
  std::cout << "Done" << std::endl;
  // Draw the path
  cv::normalize(rawHeight, bw, 0, 255, 32, CV_8UC1);
  pathnodes = open;
  for(int i = 0; i < path.size() - 1; i++) {
    cv::Point start = cv::Point((open[path[i]].x() - ox + 1)/res, (open[path[i]].y() - oy + 1)/res);
    cv::Point end = cv::Point((open[path[i+1]].x() - ox + 1)/res, (open[path[i+1]].y() - oy + 1)/res);
    cv::Scalar color = cv::Scalar(0, 0, 0);
    cv::line(bw, start, end, color);
  }
  cv::namedWindow("Path", 0);
  cv::imshow("Path", bw);
  cv::waitKey(0);
}