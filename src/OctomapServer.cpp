#include "OctomapServer.h"

using namespace octomap;
// using octomap_msgs::Octomap;
#include "Pathing2D.h"

namespace octomap_server{

  OctomapServer::OctomapServer():
    m_octree(NULL),
    m_maxRange(-1.0),
    m_worldFrameId("/map"), m_baseFrameId("base_footprint"),
    m_useHeightMap(true),
    m_colorFactor(0.8),
    m_latchedTopics(true),
    m_publishFreeSpace(false),
    m_res(0.05),
    m_treeDepth(0),
    m_maxTreeDepth(0),
    m_probHit(0.7), m_probMiss(0.4),
    m_thresMin(0.12), m_thresMax(0.97),
    m_pointcloudMinZ(-std::numeric_limits<double>::max()),
    m_pointcloudMaxZ(std::numeric_limits<double>::max()),
    m_occupancyMinZ(-std::numeric_limits<double>::max()),
    m_occupancyMaxZ(std::numeric_limits<double>::max()),
    m_minSizeX(0.0), m_minSizeY(0.0),
    m_filterSpeckles(false), m_filterGroundPlane(false),
    m_groundFilterDistance(0.04), m_groundFilterAngle(0.15), m_groundFilterPlaneDistance(0.07),
    m_compressMap(true),
    m_incrementalUpdate(false)
  {
    // initialize octomap object & params
    m_octree = new OcTree(m_res);
    m_octree->setProbHit(m_probHit);
    m_octree->setProbMiss(m_probMiss);
    m_octree->setClampingThresMin(m_thresMin);
    m_octree->setClampingThresMax(m_thresMax);
    m_treeDepth = m_octree->getTreeDepth();
    m_maxTreeDepth = m_treeDepth;
  }

  OctomapServer::~OctomapServer(){

    if (m_octree){
      m_octree = NULL;
    }

  }

  void OctomapServer::insertScan(const octomap::point3d& sensorOrigin, const PCLPointCloud& inputPc){

    if (!m_octree->coordToKeyChecked(sensorOrigin, m_updateBBXMin)
        || !m_octree->coordToKeyChecked(sensorOrigin, m_updateBBXMax))
    {
      // TODO: error output ("Could not generate Key for origin "<<sensorOrigin) 
    }

    // Generate octomap pointcloud
    Pointcloud pc;
    cout << "about to insert into different point cloud" << endl;
    cout << "len of pc " << inputPc.size() << endl;

    float minn = inputPc[0].y;
    float maxx = 0;

    for (PCLPointCloud::const_iterator it = inputPc.begin(); it != inputPc.end(); ++it){
      pc.push_back(it->x, it->y, it->z);
      if(it->y > maxx){
        maxx = it->y;
      }
      if(it->y < minn){
        minn = it->y;
      }
    }

    cout << "min " << minn << " max " << maxx << endl;
    cout << "about to insert into octomap" << endl;
    m_octree->insertPointCloud (pc, sensorOrigin, m_maxRange, true, true);
    cout << "inserted into octomap" << endl;
    m_octree->updateInnerOccupancy();
    cout << "updated inner occupancy" << endl;

    if (m_compressMap)
      m_octree->prune();
    cout << "pruned?" << endl;
  }

  void OctomapServer::handlePreNodeTraversal(){
    if (m_publish2DMap){
      // init projected 2D map:
      // TODO: move most of this stuff into c'tor and init map only once (adjust if size changes)
      double minX, minY, minZ, maxX, maxY, maxZ;
      m_octree->getMetricMin(minX, minY, minZ);
      m_octree->getMetricMax(maxX, maxY, maxZ);

      octomap::point3d minPt(minX, minY, minZ);
      octomap::point3d maxPt(maxX, maxY, maxZ);
      octomap::OcTreeKey minKey = m_octree->coordToKey(minPt, m_maxTreeDepth);
      octomap::OcTreeKey maxKey = m_octree->coordToKey(maxPt, m_maxTreeDepth);

    // TODO: debug output ("MinKey: %d %d %d / MaxKey: %d %d %d", minKey[0], minKey[1], minKey[2], maxKey[0], maxKey[1], maxKey[2]) 


      // add padding if requested (= new min/maxPts in x&y):
      double halfPaddedX = 0.5*m_minSizeX;
      double halfPaddedY = 0.5*m_minSizeY;
      minX = std::min(minX, -halfPaddedX);
      maxX = std::max(maxX, halfPaddedX);
      minY = std::min(minY, -halfPaddedY);
      maxY = std::max(maxY, halfPaddedY);
      minPt = octomap::point3d(minX, minY, minZ);
      maxPt = octomap::point3d(maxX, maxY, maxZ);

      OcTreeKey paddedMaxKey;
      if (!m_octree->coordToKeyChecked(minPt, m_maxTreeDepth, m_paddedMinKey)){
        // TODO: error output ("Could not create padded min OcTree key at %f %f %f", minPt.x(), minPt.y(), minPt.z()) 
        return;
      }
      if (!m_octree->coordToKeyChecked(maxPt, m_maxTreeDepth, paddedMaxKey)){
        // TODO: error output ("Could not create padded max OcTree key at %f %f %f", maxPt.x(), maxPt.y(), maxPt.z()) 
        return;
      }

    // TODO: debug output ("Padded MinKey: %d %d %d / padded MaxKey: %d %d %d", m_paddedMinKey[0], m_paddedMinKey[1], m_paddedMinKey[2], paddedMaxKey[0], paddedMaxKey[1], paddedMaxKey[2]) 

      // ROS_DEBUG("Padded MinKey: %d %d %d / padded MaxKey: %d %d %d", m_paddedMinKey[0], m_paddedMinKey[1], m_paddedMinKey[2], paddedMaxKey[0], paddedMaxKey[1], paddedMaxKey[2]);
      assert(paddedMaxKey[0] >= maxKey[0] && paddedMaxKey[1] >= maxKey[1]);

      m_multires2DScale = 1 << (m_treeDepth - m_maxTreeDepth);

      int mapOriginX = minKey[0] - m_paddedMinKey[0];
      int mapOriginY = minKey[1] - m_paddedMinKey[1];
      assert(mapOriginX >= 0 && mapOriginY >= 0);

      // might not exactly be min / max of octree:
      octomap::point3d origin = m_octree->keyToCoord(m_paddedMinKey, m_treeDepth);
      double gridRes = m_octree->getNodeSize(m_maxTreeDepth);
      
      // workaround for  multires. projection not working properly for inner nodes:
      // force re-building complete map
      if (m_maxTreeDepth < m_treeDepth)
        m_projectCompleteMap = true;

      }
    }
  }