// make && ./mapper ../testterrain/4.pcd
#include "OctomapServer.h"
#include "Pathing2D.h"
#include <stdio.h>
#include <pcl/visualization/cloud_viewer.h>

int main(int argc, const char *argv[])
{

  typedef pcl::PointCloud<pcl::PointXYZ> PCLPointCloud;

  const char* pointCloudLocation = argv[1];

  cout << pointCloudLocation <<endl;

  PCLPointCloud* cloud(new PCLPointCloud);

  if (pcl::io::loadPCDFile<pcl::PointXYZ>(pointCloudLocation, *cloud) == -1) //* load the file
  {
    PCL_ERROR("Couldn't read file model.pcd \n");
    return (-1);
  }
  std::cout << "Loaded "
            << cloud->width * cloud->height
            << " data points from model.pcd with the following fields: "
            << std::endl;




  // This code refuses to work on mac, don't tell Alex, it is supposed to show the map
  /*
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::io::loadPCDFile (pointCloudLocation, *cloud2);
  pcl::visualization::CloudViewer viewer("Cloud Viewer");
  //blocks until the cloud is actually rendered
  viewer.showCloud(cloud2);
  while (!viewer.wasStopped ())
  {
  }
  return 0;
  */


  // Shrinking the pointcloud for testing
  for (size_t i = 0; i < cloud->points.size(); ++i)
  {
    cloud->points[i].x = cloud->points[i].x / 10;
    cloud->points[i].y = cloud->points[i].y / 10;
    cloud->points[i].z = cloud->points[i].z / 10;
  }
  
  float totalx = 0;
  float totaly = 0;
  float totalz = 0;

  float minx = cloud->points[0].x;
  float miny = cloud->points[0].y;
  float minz = cloud->points[0].z;
  
  float maxx = cloud->points[0].x;
  float maxy = cloud->points[0].y;
  float maxz = cloud->points[0].z;

  for (size_t i = 0; i < cloud->points.size(); ++i)
  {
    float x = cloud->points[i].x;
    float y = cloud->points[i].y;
    float z = cloud->points[i].z;

    totalx += x;
    totaly += y;
    totalz += z;

    if(x > maxx){
      maxx = x;
    }
    if(x < minx){
      minx = x;
    }
    if(y > maxy){
      maxy = y;
    }
    if(y < miny){
      miny = y;
    }
    if(z > maxz){
      maxz = z;
    }
    if(z < minz){
      minz = z;
    }
  }

  cout << "total x y and z " << totalx << " " << totaly << " " << totalz << endl;

  float avgx = totalx / (cloud->width * cloud->height);
  float avgy = totaly / (cloud->width * cloud->height);
  float avgz = totalz / (cloud->width * cloud->height);

  cout << "avg x y and z " << avgx << " " << avgy << " " << avgz << endl;
  cout << "min x y and z " << minx << " " << miny << " " << minz << endl;
  cout << "max x y and z " << maxx << " " << maxy << " " << maxz << endl;


// subtracting the average for testing purposes, everything seems to be super offset
  for (size_t i = 0; i < cloud->points.size(); ++i)
  {
    cloud->points[i].x = cloud->points[i].x - avgx;
    cloud->points[i].y = cloud->points[i].y - avgy;
    cloud->points[i].z = cloud->points[i].z - avgz;
  }


  octomap_server::OctomapServer server;

  // octomap::point3d sensorOrigin(avgx, avgy, avgz);
  octomap::point3d sensorOrigin(0, 0, 0);

  server.insertScan(sensorOrigin, *cloud);

  server.handlePreNodeTraversal();

  Pathing2D p2d = Pathing2D();

  boost::shared_ptr<octomap::OcTree> tree (server.m_octree);

  p2d.start = 0;
  // Finds closest point to destination
  p2d.destpoint = octomap::point3d(-.5, -.3, -.04);
  p2d.octomapCallback(tree);
  // Printing out the path for now
  for(int i = 0; i < p2d.path.size(); i++) {
    std::cout << p2d.pathnodes[p2d.path[i]] << std::endl;
  }
}
